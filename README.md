# Nhulua

Vous trouverez ici les feuilles de personnage de Nhulua.
Il est un des mes personnages de Pathfinder.
C'est un tieffelin magus (Kensai, lamelié, écorcheur fiélon).

## Techno

Tout devrait être en Markdown (pour le texte) et en TOML (pour les données).

## Licence

- Pathfinder est une création de Paizo Publishing traduite en français par Black Book Editions.
- Ce qui doit l'être est en OGL.
- Le reste est en CC0.